#include "Aim.h"
#include <algorithm>

int Aim::checkRep(std::string newaim) {
	if (newaim == name) return 1;
	if (!Aim::requirements.empty()) {
		std::vector<std::string>::iterator it = find(Aim::requirements.begin(), Aim::requirements.end(), newaim);
		if (it != Aim::requirements.end()) return 1;
	}
	return 0;
}

int Aim::addReq(std::string newaim) {
	if (!checkRep(newaim)) {
		Aim::requirements.push_back(newaim);
		return 0;
	}
	return 1;
}

void Aim::showReqs() {
	for (auto it = Aim::requirements.begin(); it != Aim::requirements.end(); it++) {
		std::cout << *it << ' ';
	}
}

void Aim::showFull() {
	std::cout << name << " : ";
	int check = 1;
	for (auto it = Aim::requirements.begin(); it != Aim::requirements.end(); it++) {
		if (check) {
			std::cout << *it;
			check = 0;
		}
		else {
			std::cout << ", " << *it;
		}
	}
	std::cout << std::endl;
}