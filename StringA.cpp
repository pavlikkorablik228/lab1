#include "StringA.h"
#include "ctype.h"
#include "Aim.h"
#include <algorithm>

void StringA::newString() {
	Aim str(*it);
	strings.push_back(str);
	if (index < 0) index = 0;
	else index++;
}

void StringA::addSymbol() {
	strings[index].addNameSymb(*it);
}

void StringA::addReqSym() {
	buffer.push_back(*it);
}

void StringA::CheckString(std::string& string) {
	_fsm.enterStartState();
	for (it = string.begin(); it != string.end(); it++) {
		if (isdigit(*it)) _fsm.Numbers();
		else if (isalpha(*it)) _fsm.SymbolsNONum();
		else if ((*it) == '.') _fsm.SymbolsNONum();
		else if (*it == '_') _fsm.SymbolsNONum();
		else if (*it == '\t') _fsm.Spaces();
		else if (*it == ' ') _fsm.Spaces();
		else if (*it == ':') _fsm.Colon();
		else if (*it == '\n') _fsm.NewLine();
		else _fsm.Unknown();
	}
	_fsm.EOS();
}

bool StringA::isValid() {
	if (strings[index].addReq(buffer)) return false;
	return true;
}

void StringA::outError() {
	std::cout << "There is a error!" << std::endl;
}

void StringA::output() {
	std::cout << std::endl << "Read strings:" << std::endl;
	for (auto iter = strings.begin(); iter != strings.end(); iter++) {
		(*iter).showFull();
	}
}

void StringA::correctInd() {
	strings.erase(strings.begin() + index--);
}

void StringA::postProc() {
	std::vector<std::string> result;
	std::vector<std::string> names;
	if (strings.empty()) return;
	else {
		for (auto iter = strings.begin(); iter != strings.end(); iter++) {
			names.push_back((*iter).getName());
		}
		for (auto iter = strings.begin(); iter != strings.end(); iter++) {
			std::vector<std::string> tempo = (*iter).gerReq();
			for (auto iter1 = tempo.begin(); iter1 != tempo.end(); iter1++) {
				auto one = find(names.begin(), names.end(), *iter1);
				auto two = find(result.begin(), result.end(), *iter1);
				if (one == names.end() && two == result.end()) result.push_back(*iter1);
			}
		}
	}
	std::cout << std::endl << "Result aims: " << std::endl;
	for (auto iter = result.begin(); iter != result.end(); iter++) {
		std::cout << *iter << std::endl;
	}
}
