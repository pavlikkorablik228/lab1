#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Aim.h"
#include "StringA.h"
#include <string>

int getInt(int* a) {
	int n = 0;
	do {
		n = scanf_s("%d", a);
		if (n == 0) {
			printf("Error! Type integer again\n");
			scanf_s("%*[^\n]");
		}
	} while (n == 0);
	scanf_s("%*c");
	return n < 0 ? 1 : 0;
}

int main() {
	std::string initial;
	int amount;
	StringA thiscontext;
	std::cout << "Please enter the amount of strings:" << std::endl;
	while (getInt(&amount)) std::cout << "Incorrect amount of strings!!! Please try again!" << std::endl;
	while (amount--) {
		if (!initial.empty()) initial.push_back('\n');
		std::string temp;
		std::getline(std::cin, temp);
		initial.append(temp);
	}
	std::cout << std::endl << "Entered string:" << std::endl << initial << std::endl;
	thiscontext.CheckString(initial);
	thiscontext.output();
	return 0;
}