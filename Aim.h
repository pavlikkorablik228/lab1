#pragma once
#include <iostream>
#include <vector>

class Aim
{
private:
	std::string name;
	std::vector<std::string> requirements;
	int checkRep(std::string);
public:
	Aim() {};
	Aim(char s) {name.push_back(s); };
	Aim(std::string name) : name(name) {};
	~Aim() {};
	int addReq(std::string);
	std::string getName() { return name; };
	void showReqs();
	void showFull();
	void addNameSymb(char t) { name.push_back(t); }
	std::vector<std::string> gerReq() { return requirements; }
};

