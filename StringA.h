#include "StringA_sm.h"
#include "Aim.h"
#include <vector>
#include <iostream>
#define _CRT_SECURE_NO_WARNINGS

class StringA {
private:
	StringAContext _fsm;
	int index;
	std::string buffer;
	std::string::iterator it;
	std::vector<Aim> strings;
public:
	StringA() : _fsm(*this), index(-1) {};
	~StringA() {};
	void CheckString(std::string &);
	void newString();
	void addSymbol();
	void addReqSym();
	bool isValid();
	void clearbuf() { buffer.clear(); };
	void outError();
	void output();
	void correctInd();
	void postProc();
};